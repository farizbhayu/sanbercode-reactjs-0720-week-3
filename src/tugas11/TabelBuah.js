import React, { Component } from 'react';

class Nama extends Component {
    render() {
        return <td>{this.props.nama}</td> 
    }
}

class Harga extends Component {
    render() {
        return <td>{this.props.harga}</td>
    }
}

class Berat extends Component {
    render() {
        return <td>{this.props.berat / 1000} kg</td>
    }
}

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
];

class TabelBuah extends Component {
    render(){
        return (
            <>
            <center>
            <h1>Tabel Harga Buah</h1>
                <table style={{width:"50%", border: "1px solid black"}}>
                    <thead>
                        <tr style={{backgroundColor:"#aba9aa"}}>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Berat</th>
                        </tr>
                    </thead>
                    <tbody>
                    {dataHargaBuah.map( e => {
                        return (
                            <tr style={{backgroundColor: "#ff7f50"}}>
                                <Nama nama={e.nama} />
                                <Harga harga = {e.harga} />
                                <Berat berat = {e.berat}/>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
            </center>
            </>
        )
    }
}

export default TabelBuah;