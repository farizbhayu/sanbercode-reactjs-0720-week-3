import React from 'react';
import './App.css';
import TabelBuah from './tugas11/TabelBuah';
import Timer from './tugas12/Timer';
import FormBuah from './tugas13/FormBuah';
import BuahAxios from './tugas14/BuahAxios';

function App() {
  return (
    <>
      { /* Tugas 11 */ }
      {/* <TabelBuah /> */}
      {/* Tugas 12 */}
      {/* <Timer waktu = {100}/> */}
      {/* Tugas 13 */}
      {/* <FormBuah /> */}
      {/* Tugas 14 */}
      <BuahAxios />
    </>
  );
}

export default App;
